- [Gitlab] to
  * maintain (versioning) pipeline source
  * share tool CWL/WDL descriptions
  * mirror tool src
  * CI/CD docker components from tool src
- [Minio] to
  * store docker registry files (i.e. docker images)
- [Registry] to
  * store private certified docker images
- [Postgres] to
  * support [Gitlab]
